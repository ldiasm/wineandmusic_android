package br.com.wineandmusic.enobook.activities


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.wineandmusic.enobook.R


class WineMenuFragment : Fragment() {

    companion object {
        fun newInstance(): WineMenuFragment {
            val wineMenuFragment = WineMenuFragment()
            val args = Bundle()
            wineMenuFragment.arguments = args
            return wineMenuFragment
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootview = inflater.inflate(R.layout.wine_menu, container, false)
        return rootview
    }

}
