package br.com.wineandmusic.enobook.activities

import android.graphics.Color
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.SearchView
import br.com.wineandmusic.enobook.R
import br.com.wineandmusic.enobook.adapters.MyWineRecyclerViewAdapter
import br.com.wineandmusic.enobook.service.API
import br.com.wineandmusic.enobook.service.Vinho
import kotlinx.android.synthetic.main.activity_search.*


class SearchActivity : AppCompatActivity() {

    var adapter: MyWineRecyclerViewAdapter? = null

    protected var listener: WineGridFragment.OnListFragmentInteractionListener? = null

    protected var columnCount = 3

    lateinit var searchView: SearchView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        var api = API()

        api.vinhos(::setView, list, "")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        var item: MenuItem = menu!!.findItem(R.id.search)
        searchView = item.actionView as SearchView
        MenuItemCompat.setOnActionExpandListener(item, object : MenuItemCompat.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                (searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as? EditText)?.setHintTextColor(Color.BLACK)
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                searchView.setQuery("", false)
                return true
            }
        })
        searchView.maxWidth = Int.MAX_VALUE
        searchName(searchView)
        return true
    }

    private fun searchName(searchView: SearchView) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                var api = API()

                api.vinhos(::setView, list, query)

                return false
            }


            override fun onQueryTextChange(newText: String?): Boolean {

                var api = API()

                api.vinhos(::setView, list, newText)

                return true
            }

        })
    }

    open fun setView(vinhos: List<Vinho>, view: View?, filtro: String?) {

        var vinhus: List<Vinho>?

        if (!filtro.isNullOrEmpty()) {

            vinhus = vinhos.filter { v ->
                (v.nome.toString().toLowerCase().contains(filtro?.toLowerCase()!!)
                        || v.pais?.nome.toString().toLowerCase().contains(filtro.toLowerCase()))
            }


            Log.d("DEBUG", "filtrando: " + filtro + " " + vinhus.toString())
        } else {

            vinhus = vinhos

            Log.d("DEBUG", "Não filrou:" + vinhus.toString())
        }

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }

                adapter = MyWineRecyclerViewAdapter(vinhus, listener, view)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == R.id.navigation_search) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (!searchView.isIconified) {
            searchView.isIconified = true
            return
        }
        super.onBackPressed()
    }

}
