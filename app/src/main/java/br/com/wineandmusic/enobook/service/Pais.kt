package br.com.wineandmusic.enobook.service

import com.google.gson.annotations.SerializedName

class Pais {
    @SerializedName("id")
    var id: Int = 0

    @SerializedName("nome")
    var nome: String? = null
}