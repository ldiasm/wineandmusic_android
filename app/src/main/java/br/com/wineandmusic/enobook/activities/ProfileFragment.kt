package br.com.wineandmusic.enobook.activities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.wineandmusic.enobook.R

class ProfileFragment : WineGridFragment() {

    companion object {
        fun newInstance(): FavoriteWinesGridFragment {
            val favoriteWinesGridFragment = FavoriteWinesGridFragment()
            val args = Bundle()
            favoriteWinesGridFragment.arguments = args
            return favoriteWinesGridFragment
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootview = inflater.inflate(R.layout.profile_menu, container, false)
        return rootview
    }
}
