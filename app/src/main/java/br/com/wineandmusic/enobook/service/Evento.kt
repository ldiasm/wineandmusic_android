package br.com.wineandmusic.enobook.service

import com.google.gson.annotations.SerializedName

class Evento {
    @SerializedName("id")
    var id: Int = 0

    @SerializedName("nome")
    var nome: String? = null

    @SerializedName("descricao")
    var descricao: String? = null
}