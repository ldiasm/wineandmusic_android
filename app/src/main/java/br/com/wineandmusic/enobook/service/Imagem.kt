package br.com.wineandmusic.enobook.service

import com.google.gson.annotations.SerializedName

class Imagem {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("nome")
    var nome: String? = null

}