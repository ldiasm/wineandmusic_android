package br.com.wineandmusic.enobook.service

import com.google.gson.annotations.SerializedName

class Avaliacao {

    @SerializedName("nota")
    var nota: Float? = null

    @SerializedName("users_id")
    var users_id: Int? = 0

    @SerializedName("vinho_id")
    var vinho_id: Int? = 0

    @SerializedName("observacao")
    var observacao: String? = null
}