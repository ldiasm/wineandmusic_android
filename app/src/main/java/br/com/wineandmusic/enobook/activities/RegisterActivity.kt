package br.com.wineandmusic.enobook.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import br.com.wineandmusic.enobook.R
import br.com.wineandmusic.enobook.service.API
import br.com.wineandmusic.enobook.service.Login

class RegisterActivity : AppCompatActivity(), View.OnClickListener {

    private var userName: EditText? = null
    private var userMail: EditText? = null
    private var userPhone: EditText? = null
    private var userPassword: EditText? = null
    private var userPasswordConfirm: EditText? = null
    private var button: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        userName = findViewById(R.id.userName)
        userMail = findViewById(R.id.userMail)
        userPhone = findViewById(R.id.userPhone)
        userPassword = findViewById(R.id.userPassword)
        userPasswordConfirm = findViewById(R.id.userPasswordConfirm)

        button = findViewById(R.id.button_register_confirm)
        button?.setOnClickListener(this)

    }

    override fun onClick(view: View) {

        var cancel: Boolean = false

        //validando nome
        if (userName?.text.isNullOrEmpty()) {

            userName?.error = getString(R.string.error_field_required)
            userName?.requestFocus()
            cancel = true
        }

        //validando telefone
        if (userPhone?.text.isNullOrEmpty()) {

            userPhone?.error = getString(R.string.error_field_required)
            userPhone?.requestFocus()
            cancel = true
        }

        // validando e-mail
        if (TextUtils.isEmpty(userMail?.text)) {
            userMail?.error = getString(R.string.error_field_required)
            userMail?.requestFocus()

            cancel = true

        } else if (!isEmailValid(userMail?.text.toString())) {
            userMail?.error = getString(R.string.error_invalid_email)
            userMail?.requestFocus()

            cancel = true
        }

        // validando senha
        if (TextUtils.isEmpty(userPassword?.text) || !isPasswordValid(userPassword?.text.toString())) {
            userPassword?.error = getString(R.string.error_invalid_password)
            userPassword?.requestFocus()

            cancel = true
        }

        if (TextUtils.isEmpty(userPasswordConfirm?.text) || !isPasswordValid(userPasswordConfirm?.text.toString())) {
            userPasswordConfirm?.error = getString(R.string.error_invalid_password)
            userPasswordConfirm?.requestFocus()

            cancel = true
        }

        if (userPassword?.text.toString() != userPasswordConfirm?.text.toString()) {
            userPasswordConfirm?.error = "As senhas não conferem!"
            userPasswordConfirm?.requestFocus()

            cancel = true
        }

        if (!cancel) {

            var api: API = API()

            api.cadastrar(
                    userName?.text.toString(),
                    userPhone?.text.toString(),
                    userMail?.text.toString(),
                    userPassword?.text.toString(),
                    this,
                    ::sucesso,
                    ::falha
            )
        }
    }

    private fun isPasswordValid(password: String): Boolean {
        //TODO: Replace this with your own logic
        return password.length >= 6
    }

    private fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    fun sucesso(login: Login?) {

        Log.d("DEBUG", "cadastro com sucesso! " + login?.access_token)

        val openHowToActivity = Intent(this, HowToActivity::class.java)
        startActivity(openHowToActivity)
    }

    fun falha(t: Throwable) {

        Log.d("DEBUG", "Falha no cadastro!" + t.toString())
    }
}
