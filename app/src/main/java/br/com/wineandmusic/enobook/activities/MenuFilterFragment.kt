package br.com.wineandmusic.enobook.activities


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import br.com.wineandmusic.enobook.R
import br.com.wineandmusic.enobook.adapters.MyWineRecyclerViewAdapter

class MenuFilterFragment : Fragment() {

    lateinit var adapter: MyWineRecyclerViewAdapter
    lateinit var searchView: SearchView
    protected var listener: WineGridFragment.OnListFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var rootView = inflater.inflate(R.layout.menu_filter, container, false)
        return rootView
    }
}