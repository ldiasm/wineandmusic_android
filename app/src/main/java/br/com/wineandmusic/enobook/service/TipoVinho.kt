package br.com.wineandmusic.enobook.service

import com.google.gson.annotations.SerializedName

class TipoVinho {
    @SerializedName("id")
    var id: Int = 0

    @SerializedName("descricao")
    var descricao: String? = null
}