package br.com.wineandmusic.enobook.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.FrameLayout
import br.com.wineandmusic.enobook.R
import br.com.wineandmusic.enobook.service.Vinho

class BottomNavigationActivity : AppCompatActivity(), WineGridFragment.OnListFragmentInteractionListener {
    override fun onListFragmentInteraction(item: Vinho?) {

    }

    private var content: FrameLayout? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
        /*R.id.navigation_schedule -> {
            val fragment = EventScheduleFragment.newInstance()
            addFragment(fragment)
            return@OnNavigationItemSelectedListener true
        }*/
            R.id.navigation_menu -> {
                val fragment = WineMenuFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                //TODO: Fazer o filtro funcionar como fragment, ao invés de activity
                /*val fragment = MenuFilterFragment()
                addFragment(fragment)*/
                var openSearchActivity = Intent(this, SearchActivity::class.java)
                startActivity(openSearchActivity)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                val fragment = ProfileFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    /**
     * add/replace fragment in container [framelayout]
     */
    @SuppressLint("PrivateResource")
    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
                .replace(R.id.wine_image, fragment, fragment.javaClass.simpleName)
                .addToBackStack(fragment.javaClass.simpleName)
                .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation)

        content = findViewById(R.id.wine_image)
        val navigation = findViewById<BottomNavigationView>(R.id.navigation)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        val fragment = WineMenuFragment.newInstance()
        addFragment(fragment)
    }
}
