package br.com.wineandmusic.enobook.adapters


import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import br.com.wineandmusic.enobook.R
import br.com.wineandmusic.enobook.activities.WineDetailsActivity
import br.com.wineandmusic.enobook.activities.WineGridFragment.OnListFragmentInteractionListener
import br.com.wineandmusic.enobook.service.Vinho
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.wine_card.view.*


/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class MyWineRecyclerViewAdapter(
        private val mValues: List<Vinho>,
        private val mListener: OnListFragmentInteractionListener?,
        val view: View)
    : RecyclerView.Adapter<MyWineRecyclerViewAdapter.ViewHolder>(), Filterable {

    private val mOnClickListener: View.OnClickListener
    private lateinit var filteredList: List<Vinho>

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Vinho

            Toast.makeText(v.context, "${item.nome}", Toast.LENGTH_SHORT).show()
            run({
                val openWineDetailsActivity: Intent
                when (item.id) {
                    item.id -> {
                        openWineDetailsActivity = Intent(v.context, WineDetailsActivity::class.java)

                        openWineDetailsActivity.putExtra("foto", item.imagem?.nome)
                        openWineDetailsActivity.putExtra("id", item.id)
                        openWineDetailsActivity.putExtra("nome", item.nome)
                        openWineDetailsActivity.putExtra("pais", item.pais?.nome)
                        openWineDetailsActivity.putExtra("nota", item.nota)
                        openWineDetailsActivity.putExtra("tipo", item.tipo?.descricao)
                        openWineDetailsActivity.putExtra("safra", item.safra)
                        openWineDetailsActivity.putExtra("uva", item.uva?.nome)
                        openWineDetailsActivity.putExtra("descricao", item.descricao)
                        openWineDetailsActivity.putExtra("comentario", item.observacao)

                        v.context.startActivity(openWineDetailsActivity)
                    }
                    else -> {
                        Toast.makeText(view.context, "Ocorreu um erro ao trazer as informações de: ${mValues.indexOf(item)}", Toast.LENGTH_SHORT).show()
                    }
                }
            })

            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.wine_card, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        //nome do vinho
        holder.mIdView.text = item.nome
        //holder.nota.numStars = 5
        holder.nota.rating = item.nota

        holder.nota.setIsIndicator(true)

        //foto do vinho
        Glide.with(holder.mView).load(item.imagem?.nome)
                //.placeholder(item.imagem?.nome)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.mContentView)

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIdView: TextView = mView.wine_description
        val mContentView: ImageView = mView.wine_image
        var nota: RatingBar = mView.nota
    }

    /*
    override fun toString(): String {
        return super.toString() + " '" + mContentView.text + "'"
    }
    */

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                var charString: String = constraint.toString()
                if (charString.isEmpty()) {
                    filteredList = mValues as MutableList<Vinho>
                } else {
                    var filteredList2: MutableList<Vinho> = mutableListOf()
                    for (wine: Vinho in mValues) {
                        if (wine.nome.toString().toLowerCase().contains(charString.toLowerCase())
                                || wine.pais.toString().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList2.add(wine)
                        }
                    }
                    filteredList = filteredList2
                }
                var filterResults = FilterResults()
                filterResults.values = filteredList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredList = results!!.values as List<Vinho>
                notifyDataSetChanged()
            }

        }
    }
}
