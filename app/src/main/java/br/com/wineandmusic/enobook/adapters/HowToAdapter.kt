package br.com.wineandmusic.enobook.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import br.com.wineandmusic.enobook.R
import br.com.wineandmusic.enobook.activities.BottomNavigationActivity


class HowToAdapter(var context: Context) : PagerAdapter() {

    private val howToImages = intArrayOf(R.drawable.passo_1, R.drawable.passo_2, R.drawable.passo_3_sem_botao, R.drawable.passo_extra)
    var openBottomNavigationActivity = Intent(context, BottomNavigationActivity::class.java)
    override fun getCount(): Int {
        return howToImages.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as ImageView
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageView = ImageView(context)
        container.setBackgroundColor(Color.BLACK)
        imageView.scaleType = ImageView.ScaleType.CENTER_INSIDE
        imageView.setImageResource(howToImages[position])
        (container as ViewPager).addView(imageView, 0)
        return imageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as ImageView)
        if (position == 1){
            context.startActivity(openBottomNavigationActivity)
            Toast.makeText(context, "Bem vindo(a)!", Toast.LENGTH_SHORT).show()
        }
    }

}