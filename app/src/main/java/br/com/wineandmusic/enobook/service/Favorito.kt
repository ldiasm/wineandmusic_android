package br.com.wineandmusic.enobook.service

import com.google.gson.annotations.SerializedName

class Favorito {

    @SerializedName("vinho_id")
    var vinho_id: Int? = null

    @SerializedName("nota")
    var nota: Int? = null

    @SerializedName("observacao")
    var observacao: String? = null
}