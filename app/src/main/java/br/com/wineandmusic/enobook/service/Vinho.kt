package br.com.wineandmusic.enobook.service

import com.google.gson.annotations.SerializedName

class Vinho {

    @SerializedName("id")
    var id: Int = 0

    @SerializedName("foto_arquivo_id")
    var foto_arquivo_id: Int? = null

    @SerializedName("nome")
    var nome: String? = null

    @SerializedName("descricao")
    var descricao: String? = null

    @SerializedName("teor_alcolico")
    var teor_alcolico: Float = 0f

    @SerializedName("safra")
    var safra: String? = null

    @SerializedName("pais_id")
    var pais_id: Int? = null

    @SerializedName("tipo_uva_id")
    var tipo_uva_id: Int? = null

    @SerializedName("tipo_vinho_id")
    var tipo_vinho_id: Int? = null

    @SerializedName("vinicola")
    var vinicola: String? = null

    @SerializedName("pais")
    var pais: Pais? = null

    @SerializedName("tipo")
    var tipo: TipoVinho? = null

    @SerializedName("uva")
    var uva: TipoUva? = null

    @SerializedName("imagem")
    var imagem: Imagem? = null

    @SerializedName("nota")
    var nota: Float = 0f

    @SerializedName("observacao")
    var observacao: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Vinho

        if (id != other.id) return false
        if (foto_arquivo_id != other.foto_arquivo_id) return false
        if (nome != other.nome) return false
        if (descricao != other.descricao) return false
        if (teor_alcolico != other.teor_alcolico) return false
        if (safra != other.safra) return false
        if (pais_id != other.pais_id) return false
        if (tipo_uva_id != other.tipo_uva_id) return false
        if (tipo_vinho_id != other.tipo_vinho_id) return false
        if (vinicola != other.vinicola) return false
        if (pais != other.pais) return false
        if (tipo != other.tipo) return false
        if (uva != other.uva) return false
        if (imagem != other.imagem) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + (foto_arquivo_id ?: 0)
        result = 31 * result + (nome?.hashCode() ?: 0)
        result = 31 * result + (descricao?.hashCode() ?: 0)
        result = 31 * result + teor_alcolico.hashCode()
        result = 31 * result + (safra?.hashCode() ?: 0)
        result = 31 * result + (pais_id ?: 0)
        result = 31 * result + (tipo_uva_id ?: 0)
        result = 31 * result + (tipo_vinho_id ?: 0)
        result = 31 * result + (vinicola?.hashCode() ?: 0)
        result = 31 * result + (pais?.hashCode() ?: 0)
        result = 31 * result + (tipo?.hashCode() ?: 0)
        result = 31 * result + (uva?.hashCode() ?: 0)
        result = 31 * result + (imagem?.hashCode() ?: 0)
        return result
    }


}