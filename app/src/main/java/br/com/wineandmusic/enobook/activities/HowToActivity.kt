package br.com.wineandmusic.enobook.activities

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import br.com.wineandmusic.enobook.R
import br.com.wineandmusic.enobook.adapters.HowToAdapter


class HowToActivity : AppCompatActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_how)

        val viewPager = findViewById<View>(R.id.view_pager) as ViewPager
        val adapter = HowToAdapter(this)
        viewPager.adapter = adapter
    }

}
