package br.com.wineandmusic.enobook.service

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.View
import br.com.wineandmusic.enobook.R
import br.com.wineandmusic.enobook.retrofit.RetrofitInitializer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.preference.PreferenceManager



class API {

    protected var vinhosCache: List<Vinho> = emptyList()

    /**
     * Exemplo de uso com lambda como callback:
        api.vinhos({
            vinhos->
            for (vinho in vinhos) {
                Log.d("STATE", "for: " + vinho.pais?.nome.toString())
                Log.d("STATE", "for: " + vinho.tipo?.descricao.toString())
                Log.d("STATE", "for: " + vinho.uva?.nome.toString())
                Log.d("STATE", "for: " + vinho.imagem?.nome.toString())
            }
        })

        ou

        definição da função (pode ser numa classe):

            fun callback (vinhos: List<Vinho>)
            {
                for (vinho in vinhos) {
                Log.d("STATE", "for: " + vinho.pais?.nome.toString())
                Log.d("STATE", "for: " + vinho.tipo?.descricao.toString())
                Log.d("STATE", "for: " + vinho.uva?.nome.toString())
                Log.d("STATE", "for: " + vinho.imagem?.nome.toString())
                }
            }

        e chamada da função:

            api.vinhos(::callback)
     */
    fun vinhos (callback: (List<Vinho>, view: View?, filtro:String?) -> Unit, view:View?, filtro: String?) {
        var call = RetrofitInitializer().webService()

        val token:String? = getToken(view?.context)

        if (vinhosCache.isEmpty()) {

            call.vinhos(
                    "Bearer " + token
            ).enqueue(object: Callback<List<Vinho>?> {

                override fun onResponse(call: Call<List<Vinho>?>?,
                                        response: Response<List<Vinho>?>?) {

                    response?.body()?.let {
                        val vinhos: List<Vinho> = it

                        vinhosCache = vinhos

                        //disparando callback
                        callback(vinhos,view, filtro)
                    }
                }

                override fun onFailure(call: Call<List<Vinho>?>?,
                                       t: Throwable?) {
                    Log.d("STATE", "onFailure:" + t.toString());
                }
            })

        } else {
            callback(vinhosCache,view, filtro)
        }
    }

    /**
     * Exemplo de uso:
         api.paises({
            paises->
            for (pais in paises) {
                Log.d("STATE", "paises: " + pais?.nome.toString());
            }
        })
     */
    fun paises (callback: (List<Pais>) -> Unit) {
        var call = RetrofitInitializer().webService()

        Log.d("STATE", "loaded");

        call.paises().enqueue(object: Callback<List<Pais>?> {

            override fun onResponse(call: Call<List<Pais>?>?,
                                    response: Response<List<Pais>?>?) {

                response?.body()?.let {
                    val paises: List<Pais> = it

                    callback(paises)
                }
            }

            override fun onFailure(call: Call<List<Pais>?>?,
                                   t: Throwable?) {
                Log.d("STATE", "onFailure:" + t.toString());
            }
        })
    }

    /**
     * Exemplo:
      api.tiposVinhos({
        tiposVinhos->
        for (tipoVinho in tiposVinhos) {
            Log.d("STATE", "tipos de vinho: " + tipoVinho?.descricao);
        }
      })
     */
    fun tiposVinhos (callback: (List<TipoVinho>) -> Unit) {
        var call = RetrofitInitializer().webService()

        call.tiposVinhos().enqueue(object: Callback<List<TipoVinho>?> {

            override fun onResponse(call: Call<List<TipoVinho>?>?,
                                    response: Response<List<TipoVinho>?>?) {

                response?.body()?.let {
                    val tiposVinhos: List<TipoVinho> = it

                    callback(tiposVinhos)
                }
            }

            override fun onFailure(call: Call<List<TipoVinho>?>?,
                                   t: Throwable?) {
                Log.d("STATE", "onFailure:" + t.toString());
            }
        })
    }

    /**
     * Exemplo:
        api.tiposUvas({
            tiposUvas->
            for (tipoUva in tiposUvas) {
                Log.d("STATE", "tipo uva: " + tipoUva?.nome);
            }
        })
     */
    fun tiposUvas (callback: (List<TipoUva>) -> Unit) {
        var call = RetrofitInitializer().webService()

        call.tiposUvas().enqueue(object: Callback<List<TipoUva>?> {

            override fun onResponse(call: Call<List<TipoUva>?>?,
                                    response: Response<List<TipoUva>?>?) {

                response?.body()?.let {
                    val tiposUvas: List<TipoUva> = it

                    callback(tiposUvas)
                }
            }

            override fun onFailure(call: Call<List<TipoUva>?>?,
                                   t: Throwable?) {
                Log.d("STATE", "onFailure:" + t.toString());
            }
        })
    }

    /**
     * Exemplo:
        api.eventos({
            eventos->
            for (evento in eventos) {
                Log.d("STATE", "eventos: " + evento?.nome);
            }
        })
     */
    fun eventos (callback: (List<Evento>) -> Unit) {

        var call = RetrofitInitializer().webService()

        call.eventos().enqueue(object: Callback<List<Evento>?> {

            override fun onResponse(call: Call<List<Evento>?>?,
                                    response: Response<List<Evento>?>?) {

                response?.body()?.let {
                    val eventos: List<Evento> = it

                    callback(eventos)
                }
            }

            override fun onFailure(call: Call<List<Evento>?>?,
                                   t: Throwable?) {
                Log.d("STATE", "onFailure:" + t.toString());
            }
        })
    }

    fun cadastrar (nome:String, telefone:String, email:String, senha: String, context: Context?, sucesso: (Login?) -> Unit, falha: (t: Throwable) -> Unit ) {

        var call = RetrofitInitializer().webService()

        call
        .cadastrar(
                nome,
                email,
                senha,
                telefone
        )
        .enqueue(object : Callback<Login> {

            override fun onResponse(call: Call<Login>, response: Response<Login>) {

                if (response.isSuccessful()) {

                    var login:Login? = response?.body()

                    if (login?.access_token.isNullOrEmpty()) {

                        val t = Throwable("Dados inválidos")
                        falha(t)

                    } else {
                        salvarUsuario(login, context)
                        sucesso(login)
                    }
                }
            }

            override fun onFailure(call: Call<Login>, t: Throwable) {

                falha(t)
            }
        })
    }

    fun login (email: String, senha :String, context: Context?, sucesso: (Login?) -> Unit, falha: (t: Throwable) -> Unit ) {

        var call = RetrofitInitializer().webService()

        call
        .login(
                email,
                senha
        )
        .enqueue(object : Callback<Login> {

            override fun onResponse(call: Call<Login>, response: Response<Login>) {

                if (response.isSuccessful()) {

                    var login:Login? = response?.body()

                    salvarUsuario(login, context)
                    sucesso(login)
                }
            }

            override fun onFailure(call: Call<Login>, t: Throwable) {

                falha(t)
            }
        })
    }

    fun avaliar (vinho:Int, nota: Int, obs:String? , view: View?, sucesso: (avaliacao: Avaliacao?) -> Unit, falha: (t: Throwable) -> Unit) {

        val token:String? = getToken(view?.context)

        var call = RetrofitInitializer().webService()

        call
        .avaliar(
                "Bearer " + token,
                nota,
                vinho,
                obs
        )
        .enqueue(object : Callback<Avaliacao> {

            override fun onResponse(call: Call<Avaliacao>, response: Response<Avaliacao>) {

                if (response.isSuccessful()) {

                    var avaliacao:Avaliacao? = response?.body()

                    //limpando cache dos vinhos para puxar os dados com a nova avaliação
                    vinhosCache = emptyList()

                    sucesso(avaliacao)
                }
            }

            override fun onFailure(call: Call<Avaliacao>, t: Throwable) {

                falha(t)
            }
        })
    }

    fun favoritos (callback: (List<Vinho>, view: View?, filtro:String?) -> Unit, view:View?) {
        val token:String? = getToken(view?.context)

        var call = RetrofitInitializer().webService()

        call
        .favoritos(
                "Bearer " + token,
                1
        )
        .enqueue(object: Callback<List<Vinho>?> {

            override fun onResponse(call: Call<List<Vinho>?>?,
                                    response: Response<List<Vinho>?>?) {

                response?.body()?.let {
                    val vinhos: List<Vinho> = it

                    callback(vinhos,view, "")
                }
            }

            override fun onFailure(call: Call<List<Vinho>?>?,
                                   t: Throwable?) {
                Log.d("STATE", "onFailure:" + t.toString())
            }
        })
    }

    fun salvarUsuario (login:Login?, context: Context?)
    {
        //var sharedPreferences : SharedPreferences? = context?.getSharedPreferences(PREFS, Context.MODE_PRIVATE)
        //var editor:SharedPreferences.Editor? = sharedPreferences?.edit()

        val preferences = PreferenceManager.getDefaultSharedPreferences(context)

        var editor:SharedPreferences.Editor? = preferences?.edit()

        editor?.putString("access_token", login?.access_token)
        editor?.putString("refresh_token", login?.refresh_token)

        editor?.commit()

        Log.d("DEBUG", "write token:" + login?.access_token)
    }

    fun getToken (context: Context?) : String?
    {
        //var sharedPreferences : SharedPreferences? = context?.getSharedPreferences(PREFS, Context.MODE_PRIVATE)

        //val token : String? = sharedPreferences?.getString(PREFS,"access_token")

        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val token = preferences.getString("access_token", "")

        Log.d("DEBUG", "read token:" + token)

        return token
    }
}