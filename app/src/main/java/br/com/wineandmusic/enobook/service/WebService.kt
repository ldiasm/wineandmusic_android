package br.com.wineandmusic.enobook.service

import retrofit2.Call
import retrofit2.http.*

interface WebService {
    @POST("api/vinhos")
    fun vinhos(
            @Header("Authorization") header: String
    ): Call<List<Vinho>>

    @GET("api/paises")
    fun paises(): Call<List<Pais>>

    @GET("api/tiposdevinhos")
    fun tiposVinhos(): Call<List<TipoVinho>>

    @GET("api/tiposdeuvas")
    fun tiposUvas(): Call<List<TipoUva>>

    @GET("api/eventos")
    fun eventos(): Call<List<Evento>>

    @POST("api/cadastrar")
    @FormUrlEncoded
    fun cadastrar(
            @Field("nome") nome: String,
            @Field("email") email: String,
            @Field("senha") senha: String,
            @Field("telefone") telefone: String
    ): Call<Login>

    @POST("api/login")
    @FormUrlEncoded
    fun login(
            @Field("email") email: String,
            @Field("senha") senha: String
    ): Call<Login>

    @POST("api/avaliar")
    @FormUrlEncoded
    fun avaliar(
            @Header("Authorization") header: String,
            @Field("nota") nota: Int,
            @Field("vinho") vinho: Int,
            @Field("observacao") observacao: String? = null
    ): Call<Avaliacao>

    @POST("api/favoritos")
    @FormUrlEncoded
    fun favoritos(
            @Header("Authorization") header: String,
            @Field("completo") completo : Int
    ): Call<List<Vinho>>
}