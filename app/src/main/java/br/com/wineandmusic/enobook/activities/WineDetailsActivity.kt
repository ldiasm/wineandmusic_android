package br.com.wineandmusic.enobook.activities


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.util.Log
import android.view.View
import android.widget.Toast
import br.com.wineandmusic.enobook.R
import br.com.wineandmusic.enobook.service.API
import br.com.wineandmusic.enobook.service.Avaliacao
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.wine_details.*



class WineDetailsActivity : AppCompatActivity(), View.OnClickListener {

    var id:Int = 0
    var edit:Editable? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.wine_details)

        val bundle = intent.extras

        //foto do vinho
        Glide.with(wine_image).load(bundle.getString("foto"))
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(wine_image)

        id = bundle.getInt("id")

        wine_name.text = bundle.getString("nome")
        wine_country.text = bundle.getString("pais")
        wine_tipo.text = bundle.getString("tipo")
        wine_safra.text = bundle.getString("safra")
        wine_uva.text = bundle.getString("uva")
        wine_description.text = bundle.getString("descricao")
        wine_rate.rating = bundle.getFloat("nota")

        wine_comment_text.setText(bundle.getString("comentario"))

        salvar.setOnClickListener(this)

        Log.d("DEBUG", bundle.toString())

    }

    override fun onClick(view: View){

        val api: API = API()

        Log.d("DEBUG",
                    id.toString()
                +" "+
                        wine_rate.rating
                +" "+
                        wine_comment_text.text.toString()
        )


        api.avaliar(
                id,
                wine_rate.rating.toInt(),
                wine_comment_text.text.toString(),
                view,
                ::sucesso,
                ::falha
        )
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.d("CDA", "onBackPressed Called")
        val fm = fragmentManager
        if (fm.backStackEntryCount > 0) {
            Log.i("Activity Anterior", "popping backstack")
            fm.popBackStack()
        } else {
            Log.i("Activity Anterior", "nothing on backstack, calling super")
            super.onBackPressed()
        }
    }

    fun sucesso (avaliacao: Avaliacao?) {

        Log.d("DEBUG", "Avaliado com sucesso! : " + avaliacao.toString())

        Toast.makeText(this, "Avaliação salva!", Toast.LENGTH_SHORT).show()

    }

    fun falha (t: Throwable) {

        Log.d("DEBUG", "falha ao enviar avaliação: " + t.toString())

        Toast.makeText(this, "Ocorreu um erro! Não foi possível salvar suas alterações!", Toast.LENGTH_SHORT).show()
    }

}



