package br.com.wineandmusic.enobook.service

import com.google.gson.annotations.SerializedName

class Login {

    @SerializedName("token_type")
    var token_type: String? = null

    @SerializedName("expires_in")
    var expires_in: Int? = null

    @SerializedName("access_token")
    var access_token: String? = null

    @SerializedName("refresh_token")
    var refresh_token: String? = null
}